import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {EntryModel} from '../models/entry.model';

@Injectable()
export class APIService {

  constructor(
    private http: HttpClient
  ) { }

  getEntries(length?: number) {
    let params = new HttpParams();
    if (length || length === 0) {
      params = params.set('length', length.toString());
    }
    return this.http.get<EntryModel[]>(`${environment.baseURL}/data`, {
      params: params
    }).toPromise();
  }
}
