import { Component, OnInit } from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {EntryModel} from '../models/entry.model';

@Component({
  selector: 'nobel-entry-detail',
  templateUrl: './entry-detail.component.html',
  styleUrls: ['./entry-detail.component.sass']
})
export class EntryDetailComponent implements OnInit {
  entry: EntryModel;

  constructor(
    public bsModalRef: BsModalRef
  ) { }

  ngOnInit() {
  }

  getDataType(type) {
    const whatToDisplay = this.entry.data.filter(x => x.type === type).map(x => x.value).join(', ')
    return whatToDisplay.length > 0 ? whatToDisplay : '-';
  }
}
