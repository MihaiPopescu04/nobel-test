import {DataModel} from './data.model';

export class EntryModel {
  date: Date;
  objectId: string;
  data = new Array<DataModel>();

  constructor(date: Date, objectId: string, data: Array<DataModel>) {
    this.date = date;
    this.objectId = objectId;
    this.data = data.map(x => new DataModel(x.value, x.type));
  }
}
