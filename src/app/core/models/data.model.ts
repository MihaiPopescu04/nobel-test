export class DataModel {
  value: number;
  type: number;

  constructor(value: number, type: number) {
    this.value = value;
    this.type = type;
  }
}
