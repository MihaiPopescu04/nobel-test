import { Injectable } from '@angular/core';
import {CanActivate, CanActivateChild, Router} from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild {

  constructor(
    private router: Router
  ) { }

  canActivate() {
    if (sessionStorage.getItem('isAuthenticated')) {
      console.log('logged in');
      return true;
    } else {
      console.log('not logged in');
      this.router.navigate(['/login']);
    }
    return false;
  }

  canActivateChild() {
    return true;
  }
}
