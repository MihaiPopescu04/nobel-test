import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../auth/services/auth.service';

@Component({
  selector: 'nobel-common',
  templateUrl: './common.component.html',
  styleUrls: ['./common.component.sass']
})
export class CommonComponent implements OnInit {

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
  }

  logout() {
    this.authService.logout().then(response => {
      this.router.navigate(['/login']);
    }, error => {
      console.log('some error occurred on logout');
    });
  }
}
