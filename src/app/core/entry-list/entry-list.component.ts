import { Component, OnInit } from '@angular/core';
import {APIService} from '../services/api.service';
import {EntryModel} from '../models/entry.model';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {EntryDetailComponent} from '../entry-detail/entry-detail.component';
import {FormControl} from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Component({
  selector: 'nobel-item-list',
  templateUrl: './entry-list.component.html',
  styleUrls: ['./entry-list.component.sass']
})
export class EntryListComponent implements OnInit {

  entries: Array<EntryModel>;
  bsModalRef: BsModalRef;

  responseLengthControl = new FormControl();
  RESPONSE_LENGTH_DELAY = 1000;
  error;

  constructor(
    private api: APIService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.getEntries();
    this.responseLengthControl.valueChanges.debounceTime(this.RESPONSE_LENGTH_DELAY).distinctUntilChanged().subscribe(value => {
      this.getEntries(value);
    });
  }

  getData(entry) {
    return `[${entry.filter(x => x.type === 0).length},
      ${entry.filter(x => x.type === 1).length},
      ${entry.filter(x => x.type === 2).length}]`;
  }

  openModalForEntry(entry) {
    const initialState = {
      entry: entry
    };
    this.bsModalRef = this.modalService.show(EntryDetailComponent, {initialState: initialState, class: 'modal-lg'});
  }

  // setResponseLength() {
  //   this.api.getEntries(this.responseLengthControl.value);
  // }

  getEntries(length?) {
    this.entries = undefined;
    this.api.getEntries(length).then(response => {
      this.error = '';
      this.entries = response.map(x => new EntryModel(x.date, x.objectId, x.data));
    }, error => {
      this.entries = [];
      this.error = 'Sorry, but the server is offline';
    });
  }

  keyIsAllowed(event) {
    const keysAllowed = '0123456789';
    const code = event.keyCode;

    // Allows left and right arrows, backspace, and delete
    if (code === 37 || code === 39 || code === 8 || code === 46) {
      return;
    }
    if (keysAllowed.indexOf(event.key) === -1) {
      event.preventDefault();
    }
  }

  sort(direction) {
    this.entries.sort((a: EntryModel, b: EntryModel) => {
      if (direction === 'up') {
        return a.date > b.date ? 1 : -1;
      }
      return a.date > b.date ? -1 : 1;
    });
  }
}
