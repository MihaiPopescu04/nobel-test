import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthGuardService} from './guards/auth-guard.service';
import { CommonComponent } from './common/common.component';
import {RouterModule} from '@angular/router';
import {APIService} from './services/api.service';
import {ModalModule} from 'ngx-bootstrap';
import {EntryListComponent} from './entry-list/entry-list.component';
import {EntryDetailComponent} from './entry-detail/entry-detail.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,

    /* Bootstrap */
    ModalModule.forRoot(),
  ],
  providers: [AuthGuardService, APIService],
  declarations: [EntryListComponent, CommonComponent, EntryDetailComponent],
  exports: [EntryListComponent],
  entryComponents: [EntryDetailComponent]
})
export class CoreModule { }
