import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'nobel-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  error;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    if (sessionStorage.getItem('isAuthenticated')) {
      this.router.navigate(['/core']);
    }
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.pattern('[A-Za-z]+')]],
      password: ['', [Validators.required, Validators.pattern('[A-Za-z]+')]],
    });
  }

  onSubmit() {
    this.authService.login(this.loginForm.value.username, this.loginForm.value.password).then(response => {
      if (response['success']) {
        this.error = '';
        sessionStorage.setItem('isAuthenticated', 'true');
        this.router.navigate(['/core']);
      } else {
        this.error = 'Invalid credentials';
      }
    }, error => {
      this.error = 'Sorry, but the server is offline';
    });
  }
}
