import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable()
export class AuthService {

  constructor(
    private http: HttpClient
  ) { }

  login(username: string, password: string) {
    return this.http.post(`${environment.baseURL}/login`, {
      username: username,
      password: password
    }).toPromise();
  }

  logout() {
    // call backend for logout
    sessionStorage.removeItem('isAuthenticated');
    return Promise.resolve(true);
  }
}
