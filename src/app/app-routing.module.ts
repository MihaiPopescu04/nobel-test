import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {LoginComponent} from './auth/login/login.component';
import {AuthGuardService} from './core/guards/auth-guard.service';
import {CommonComponent} from './core/common/common.component';
import {EntryListComponent} from './core/entry-list/entry-list.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  { path: 'login', component: LoginComponent },
  { path: 'core', component: CommonComponent, canActivate: [AuthGuardService], children: [
    { path: '', pathMatch: 'full', redirectTo: 'list'},
    { path: 'list', component: EntryListComponent }
  ]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
