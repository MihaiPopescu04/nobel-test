import {Component, OnInit} from '@angular/core';
import {APIService} from './core/services/api.service';
import {EntryModel} from './core/models/entry.model';


@Component({
  selector: 'nobel-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

}
